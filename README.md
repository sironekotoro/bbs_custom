# NAME

sironekotoro/bbs_custom.pl - Perl入学式 Webアプリ編 作例

# SCREENSHOT

![スクリーンショット](https://bitbucket.org/sironekotoro/bbs_custom/raw/58330489bd648ee4693943c72a6ad653682d3f63/bbs_custom_screenshot.png)

# SYNOPSIS

    $ morbo bbs_custom.pl

or

    $ plackup --port 3000 bbs_custom.pl

or

    $ docker run --rm -p 3000:3000 --name bbs_custom -e PORT=3000 sironekotoro/bbs_custom

ブラウザで http://127.0.0.1:3000 にアクセスして画面を確認します。

morbo と plackup で起動した場合は `Ctrl + C` で終了できます。

docker で起動した場合は `$ docker stop bbs_custom` で終了します。

# DESCRIPTION

[Perl入学式](http://www.perl-entrance.org/) Webアプリ編を応用した作例です。

Perlの他に以下を利用しています。

- [クジラ 百人一首 API](http://api.aoikujira.com/hyakunin/)

- デザインとしてCSSフレームワークの[Bootstrap](https://getbootstrap.com/)

- 和歌の埋め込みにJavaScriptフレームワークの[Vue.js](https://jp.vuejs.org/index.html)

# LICENSE

Copyright (C) sironekotoro.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

sironekotoro <develop@sironekotoro.com>