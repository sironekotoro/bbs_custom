#!/usr/bin/env perl
use Mojolicious::Lite;
use Storable qw/nstore retrieve/;

# Perl入学式 2019 第5回 最終問題
# https://github.com/perl-entrance-org/workshop-2019/blob/master/5th/slide.md#%E6%9C%80%E7%B5%82%E5%95%8F%E9%A1%8C

# 書き込み内容を保存するファイル名
my $data_file = app->home->rel_file('bbs_custom_storable');

# 百人一首取得関数から百人一首を取得する
# APIの負荷を避けるため、一度だけ全短歌を取得しておく
my $POEMS = get_hundred_poems();

# 書き込み内容をファイルに保存する
sub save_file {
    my $entries = shift;

    nstore $entries, $data_file;
}

# ファイルから書き込み内容を取り出す
sub load_file {

    my $entries;

    # ファイルが存在するとき
    if ( -e $data_file ) {
        return retrieve($data_file);
    }
}

# 書き込み表示用の日時・曜日表示
sub formated_time {
    my $time = shift;
    my ( $sec, $min, $hour, $mday, $month, $year, $wday, $stime )
        = localtime($time);

    my @wdays = ( "日", "月", "火", "水", "木", "金", "土" );

    my $formated_time = sprintf(
        "%04d/%02d/%02d(%s) %02d:%02d:%02d",
        $year + 1900,
        $month + 1, $mday, $wdays[$wday], $hour, $min, $sec
    );

    return $formated_time;

}

# 百人一首をAPIから取得する
sub get_hundred_poems {

    # HTTP::Tinyを利用して百人一首を取得する
    use HTTP::Tiny;
    my $HUNDRED_POEM_API
        = 'https://api.aoikujira.com/hyakunin/get.php?fmt=json';
    my $res = HTTP::Tiny->new->get($HUNDRED_POEM_API);

 # JSONを利用して、JSONからPerlで解釈できる形式に変換する
    use JSON::PP;
    my $json = decode_json( $res->{content} );

    return $json;
}

# 百人一首の中から、ランダムに一つをJSON返す
get '/select_poem' => sub {
    my $c = shift;

    # 0〜99のランダムな数字を求める
    my $index = int( rand(100) );

    # 百人一首から一つ選ぶ
    my $select_poem = $POEMS->[$index];

    # JSON形式で返す（返した先ではajaxで解釈する）
    $c->render(
        json => {
            sakusya => $select_poem->{sakusya},
            mail    => '@example.com',
            poem    => $select_poem->{kami} . ' ' . $select_poem->{simo},
        }
    );
};

get '/' => sub {

    # 引数のコントローラーオブジェクトを受け取る
    my $c = shift;

    # サブルーチンload_fileから書き込み内容を
    # 配列リファレンスとして取得
    my $entries = load_file();

    # 配列リファレンスをテンプレート部で利用するために
    # stashに保存する
    $c->stash( entries => $entries );

    # indexテンプレートを描画する
    $c->render('index');
};

# 書き込み投稿時に呼ばれるpostメソッド
post '/post' => sub {

    # 引数のコントローラーオブジェクトを受け取る
    my $c = shift;

    # ファイルから書き込み一覧を取り出して、
    # 配列リファレンスに格納する
    my $entries = load_file();

    # 書き込み内容をコントローラから取得し
    # ハッシュに格納する
    my $time  = time();
    my %entry = (
        sakusya => $c->param('sakusya'),
        mail    => $c->param('mail'),
        poem    => $c->param('poem'),
        time    => $time,
        jtime   => formated_time($time),
    );

# ハッシュをリファレンス化して、書き込み一覧の配列リファレンスの
# 末尾に追加する
    push @{$entries}, \%entry;

    # 書き込みに通し番号をつける
    my $serial = 0;
    for my $entry ( @{$entries} ) {
        $entry->{serial} = $serial;
        $serial++;
    }

    # サブルーチンsave_fileを用いて、
    # 配列をファイルに記録する
    save_file($entries);

    # 投稿時の処理が終わったので、/ に
    # リダイレクトする
    $c->redirect_to('/');

};

app->start;

__DATA__

@@ index.html.ep
% layout 'default';
% title 'Perl入学式 2019 第5回 最終問題';

<div class="display-1">
  <h1 class="align-middle"><%= title %></h1>
  %# 元の問題文へのリンクをBootstrapでボタン風デザインにする
  <a href="https://github.com/perl-entrance-org/workshop-2019/blob/master/5th/slide.md#%E6%9C%80%E7%B5%82%E5%95%8F%E9%A1%8C" target="_blank" class="btn btn-info float-right mx-auto" >問題文</a>
</div>

<div class="display-2">
  <h2>新規投稿</h2>
</div>

<form action="/post" method="POST">
<div id="app">
  %# sakusya,mail,poem 3つのname属性のinputタグをfor文でまとめて描画する
    <div class="form-group">
      <input type="text" name="sakusya" placeholder="sakusya" class="form-control" readonly="readonly" v-model="info.sakusya">
      <input type="text" name="mail" placeholder="mail" class="form-control" readonly="readonly" v-model="info.mail">
      <input type="text" name="poem" placeholder="poem" class="form-control" readonly="readonly" v-model="info.poem">
    </div>
    <div class="form-group">
      <button type="button" class="btn btn-info" v-on:click="erase">消す</button>
      <button type="button" class="btn btn-info" v-on:click="fillPoem">和歌を変える</button>
      <button type="submit" class="btn btn-info" v-bind:disabled="isButtonDisabled">投稿する</button>
    </div>
  </div>
</form>

<div class="display-2">
  <h2>投稿一覧</h2>
</div>

<%#
 コントローラ部でstashに格納した $entriesを
 デリファレンスして配列に戻し、for文で取り出して表示する
%>

%# 投稿一覧を表示する
% for my $entry ( @{$entries} ){
  <div class="card mb-2">
    <div class="card-header row m-0">
      <div class="col-sm card-text">
        <span class="badge badge-secondary">名前</span>
        %= $entry->{sakusya}
      </div>
      <div class="col-sm card-text">
        <span class="badge badge-secondary">日時</span>
        %= $entry->{jtime}
      </div>
    </div>
    <div class="card-body card-text">
      %= $entry->{poem}
    </div>
  </div>
% }


@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
<head>
  <title><%= title %></title>

  %# bootstrapに必要となる外部リンク
  <link  rel="stylesheet" crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO">
</head>
<body class="container">
<%= content %>

%# bootstrapに必要となる外部リンク
<script crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

%# vue.jsに必要となる外部リンク
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
new Vue({
  el: '#app',
  data () {
    return {
      info: []
    }
  },
  mounted () {
    this.fillPoem()
  },
  methods:{
    erase:function(event){
      this.info.sakusya = "";
      this.info.mail    = "";
      this.info.poem    = ""},
    fillPoem: function(event) {
      axios
        .get('/select_poem')
        .then(response => (this.info = response.data))
    }
  },
  computed:{
    isButtonDisabled:function(){
      // 起動直後にcomputedが走りコンソールエラーが出るため
      if (!this.info || !this.info.sakusya ){
        return true
      }
      else if(this.info.sakusya.length > 0 && this.info.mail.length > 0 && this.info.poem.length > 0 ){
        return false
      }
    }
  }
})
</script>

</body>
</html>
